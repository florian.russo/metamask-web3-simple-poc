import './Login.css';
import React, { useState } from 'react';
import Web3 from 'web3';

let web3: Web3 | undefined = undefined; // Will hold the web3 instance

export const Login = (): JSX.Element => {
	const [loading, setLoading] = useState(false);
	const [pa, setPA] = useState(''); // hold public address

	const handleSignMessage = async ({
		publicAddress,
		message,
	}: {
		publicAddress: string;
		message: string;
	}) => {
		try {
			const signature = await web3!.eth.personal.sign(
				`I am signing my one-time nonce: ${message}`,
				publicAddress,
				'' // MetaMask will ignore the password argument here
			);

			setPA(`${publicAddress} ${signature}`);

			return { publicAddress, signature };
		} catch (err) {
			throw new Error(
				'You need to sign the message to be able to log in.'
			);
		}
	};

	const handleClick = async () => {
		// Check if MetaMask is installed
		if (!(window as any).ethereum) {
			window.alert('Please install MetaMask first.');
			return;
		}

		if (!web3) {
			try {
				// Request account access if needed
				await (window as any).ethereum.enable();

				// We don't know window.web3 version, so we use our own instance of Web3
				// with the injected provider given by MetaMask
				web3 = new Web3((window as any).ethereum);
			} catch (error) {
				window.alert('You need to allow MetaMask.');
				return;
			}
		}

		const coinbase = await web3.eth.getCoinbase();
		if (!coinbase) {
			window.alert('Please activate MetaMask first.');
			return;
		}

		console.log(web3.eth);

		const publicAddress = coinbase.toLowerCase(); // ! future breaking change can be empty if user set to incognito
		setLoading(true);

		console.log(publicAddress);

		handleSignMessage({ publicAddress, message: 'message' });
		console.log(publicAddress);
	};

	return (
		<div>
			<p>{pa}</p>
			<button className="Login-button Login-mm" onClick={handleClick}>
				{loading ? 'Loading...' : 'connect with MetaMask'}
			</button>
		</div>
	);
};
