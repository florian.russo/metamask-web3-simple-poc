import './App.css';
import React from 'react';
import { Login } from '../Login';
import logo from './logo.svg';

export const App = (): JSX.Element => {
	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />
				<h1 className="App-title">
					Welcome to Login with MetaMask Demo
				</h1>
			</header>
			<div className="App-intro">
				<Login />
			</div>
		</div>
	);
};
